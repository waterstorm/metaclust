#!/usr/bin/env python
import operator, os, pickle, sys
import cherrypy
import json
import uuid
from metacluster.lib import template
from metacluster.model import *
import shutil

localDir = os.path.dirname(__file__)
absDir = os.path.join(os.getcwd(), localDir)
cherrypy.response.timeout = 3600

class Root(object):

    def __init__(self, data):
        self.data = data

    def delete_object(self, uid):
        #remove folder
        set_dir = "./metacluster/static/data/"+uid+"/"
        if os.path.exists(set_dir):
            shutil.rmtree(set_dir)
        #remove reference
        del self.data[uid]

    @cherrypy.expose
    @template.output('index.html')
    def index(self, delete=False, cancel=False, dataset=False, myFile=False):
        if cherrypy.request.method == 'POST':
            if cancel:
                raise cherrypy.HTTPRedirect('/')
            else:
                if type(dataset) == list:
                    uid2 = dataset[1]
                    dataset = dataset[0]

                if dataset in self.data:
                    uid = dataset
                    if delete:
                        self.delete_object(uid)
                        return
                else:
                    #TODO errorpage
                    return
                if myFile:
                    megfile = MeganFile(myFile)
                    newid = str(uuid.uuid4())
                    self.data[uid].meganfiles[newid] = megfile
                    raise cherrypy.HTTPRedirect('/settings/'+uid+'/'+newid)
                else:
                    if uid2:
                        raise cherrypy.HTTPRedirect('/settings/'+uid+'/'+uid2)
                    else:
                        raise cherrypy.HTTPRedirect('/statistics/'+uid)
                
        return template.render(datasets=self.data)

    @cherrypy.expose
    @template.output('statistics.html')
    def statistics(self, uid=False, megid=False, pcadata=False, k=False, feature=False):
        if cherrypy.request.method == 'POST':
            if pcadata:
                #cluster PCA with k-means
                return self.data[uid].pca_kmeans(pcadata, k)
            elif feature:
                return self.data[uid].group_dataset(feature, self.data[uid].trans)
        if megid and uid:
            return template.render(pics=self.data[uid].meganfiles[megid].clustering, jpl=self.data[uid].jpowerlaw, pca=self.data[uid].meganfiles[megid].pca, pcoa=self.data[uid].meganfiles[megid].pcoa, kmeans=self.data[uid].meganfiles[megid].kmeans, kdev=self.data[uid].kdev, uid=uid, tax=self.data[uid].plottax, megid=megid, meg=self.data[uid].meganfiles[megid].jpowerlaw, trans=self.data[uid].trans)
        elif uid:
            return template.render(pics=self.data[uid].clustering, jpl=self.data[uid].jpowerlaw, pca=self.data[uid].pca, pcoa=self.data[uid].pcoa, kmeans=self.data[uid].kmeans, kdev=self.data[uid].kdev, uid=uid, tax=self.data[uid].plottax, trans=self.data[uid].trans, megid=False, meg=False)
        else:
            raise cherrypy.HTTPRedirect('/')
            #TODO errorpage

    @cherrypy.expose
    @template.output('visualization.html')
    def visualization(self, uid=False):
        if uid:
            return template.render(uid=uid, trans=self.data[uid].trans, dataset=self.data[uid].data)
        else:
            raise cherrypy.HTTPRedirect('/')
            #TODO errorpage

    @cherrypy.expose
    @template.output('upload.html')
    def upload(self, cancel=False, myFile=False, setname=False, descr=False, trans=False):
        if cherrypy.request.method == 'POST':
            if cancel:
                raise cherrypy.HTTPRedirect('/')
            else:
                dataset = Dataset(myFile, trans.file, setname, descr)
                newid = str(uuid.uuid4())
                self.data[newid] = dataset
                self.data[newid].meganfiles = {}
                raise cherrypy.HTTPRedirect('/settings/'+newid)
        return template.render()

    @cherrypy.expose
    @template.output('settings.html')
    def settings(self, uid=False, megid=False, sets=False, methods=False, megset=False, level=False, k=False, kinit=False, norm=False, taxtrans=False, fpt=False, feature=False, kdevdata=False):
        if cherrypy.request.method == 'POST':
            if feature != "false":
                return self.data[uid].group_dataset(feature, self.data[uid].trans)
            #fast bugfix
            if fpt == "false":
                fpt = False
            if kdevdata == "false":
                kdevdata = False
            # give all the data to the model for calculation
            if megid:
                self.data[uid].cluster(uid, sets, methods, megid, self.data[uid].meganfiles[megid], megset, level, k, kinit, norm, taxtrans, fpt, kdevdata)
            else:
                self.data[uid].cluster(uid, sets, methods, megid, False, megset, level, k, kinit, norm, taxtrans, fpt, kdevdata)
            # forwarded using jquery
        if megid and megid not in self.data:
            return template.render(uid=uid, megid=megid, dataset=self.data[uid].data, trans=self.data[uid].trans, megdata=self.data[uid].meganfiles[megid].data)
        elif megid and megid in self.data:
            if megid not in self.data[uid].meganfiles:
                megfile = MeganFile(self.data[megid])
                self.data[uid].meganfiles[megid] = megfile
            return template.render(uid=uid, megid=megid, dataset=self.data[uid].data, trans=self.data[uid].trans, megdata=self.data[uid].meganfiles[megid].data)
        return template.render(uid=uid, megid=False, dataset=self.data[uid].data, trans=self.data[uid].trans,  megdata=False)

def main():
    # define db file
    filename = "cluster.db"
    # load data from the pickle file, or initialize it to an empty list
    if os.path.exists(filename):
        fileobj = open(filename, 'rb')
        try:
            data = pickle.load(fileobj)
        finally:
            fileobj.close()
    else:
        data = {}

    def _save_data():
        # save data back to the pickle file
        fileobj = open(filename, 'wb')
        try:
            pickle.dump(data, fileobj)
        finally:
            fileobj.close()
    if hasattr(cherrypy.engine, 'subscribe'): # CherryPy >= 3.1
        cherrypy.engine.subscribe('stop', _save_data)
    else:
        cherrypy.engine.on_stop_engine_list.append(_save_data)

    # Some global configuration; note that this could be moved into a
    # configuration file
    cherrypy.config.update('config.cfg')
    #cherrypy.config.update({
        #TODO: change to 127.0.0.1
        #'server.socket_host': '10.0.7.22',
        #'server.socket_port': 8081,
        #'tools.encode.on': True,
        #'tools.encode.encoding': 'utf-8',
        #'tools.decode.on': True,
        #'tools.trailing_slash.on': True,
        #'tools.staticdir.root': os.path.abspath(os.path.dirname(__file__)),
    #})

    cherrypy.quickstart(Root(data), '/', {
        '/media': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'static'
        }
    })

if __name__ == '__main__':
    main() 
