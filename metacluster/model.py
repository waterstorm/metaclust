#! /usr/bin/env python
"""
Created on 10 April, 2014

@author: Jens Rauch
"""

##############
#  Imports   #
##############

import csv
import sys
import os
from sklearn import decomposition, cluster, datasets
import matplotlib.cm as cm
import scipy.cluster.hierarchy as shi
import scipy.spatial.distance as dist
import zipfile
import tempfile
import numpy as np
import scipy.stats as sp
import matplotlib.pyplot as plt
import pandas as pd
import shutil
import StringIO
import urllib
import cherrypy
import json
import powerlaw
from sklearn import metrics
from skbio.stats.distance import mantel
from skbio.stats.ordination import PCoA
from skbio.diversity.beta import pw_distances
from skbio.stats.spatial import procrustes


###########
# Objects #
###########

class Dataset(object):

    def __init__(self, myFile, trans, setname, descr):
        #set name and descr
        self.name = setname
        self.descr = descr
        self.data_cluster = False
        self.clustering = {}
        #save transfile into dataframe
        self.trans = pd.read_csv(trans, index_col = 0, delimiter="\t")
        #drop NaN rows
        self.trans = self.trans.dropna(axis=0, how="all")
        #create new tmp directory
        tmp_dir = tempfile.mkdtemp(dir="./tmp/")
        #check if a zip or a MEGAN file was supplied
        if myFile.filename.split(".")[1] == "zip":
            #extract all files in temp dir
            with zipfile.ZipFile(myFile.file) as myzip:
                myzip.extractall(tmp_dir)
            merged_csv = mergeMeganFolder(tmp_dir)
        elif myFile.filename.split(".")[1].lower() == "megan":
            with open(tmp_dir+"/"+myFile.filename.split(".")[0]+".megan", 'w') as newfile:
                newfile.write(myFile.file.read())
            merged_csv = mergeMeganFolder(tmp_dir)
        else:
            merged_csv = myFile.file
        #save csv as meganfile object
        self.data = pd.read_csv(merged_csv, index_col = 0, delimiter="\t")
        self.data = self.data.astype(np.float)
        #dirty fix to get rid of .0 float bug in index names
        if type(self.data.index[0]) == np.float64:
            self.data.index = map(str, map(int, self.data.index))
            self.data.index = map(int, map(str, self.data.index))
        #strip leading whitespaces in names
        self.data.columns = self.data.columns.map(lambda x: x.strip())
        #sort for better viewing later on
        self.data = self.data.sort(axis=1)
        #delete tmp folder and all data in it
        shutil.rmtree(tmp_dir)
        #drop unassigned data if present (-1)
        if -1 in self.data.index:
            self.data = self.data.drop(-1)
        if -2 in self.data.index:
            self.data = self.data.drop(-2)
        if -3 in self.data.index:
            self.data = self.data.drop(-3)
        #fix for string
        if "-1" in self.data.index:
            self.data = self.data.drop("-1")
        if "-2" in self.data.index:
            self.data = self.data.drop("-2")
        if "-3" in self.data.index:
            self.data = self.data.drop("-3")
        self.splitchar = "_"

    def cluster(self, uid, sets, methods, megid, megdata, megset, level, k, kinit, norm=False, taxtrans=False, fpt=False, kdevdata=False):
        #drop unassigned data if present (-1)
        if -1 in self.data.index:
            self.data = self.data.drop(-1)
        if -2 in self.data.index:
            self.data = self.data.drop(-2)
        if -3 in self.data.index:
            self.data = self.data.drop(-3)
        #fix for string
        if "-1" in self.data.index:
            self.data = self.data.drop("-1")
        if "-2" in self.data.index:
            self.data = self.data.drop("-2")
        if "-3" in self.data.index:
            self.data = self.data.drop("-3")
        #check if dataset directory already exists
        set_dir = "./metacluster/static/data/"+uid+"/"
        if megset:
            set_dir = "./metacluster/static/data/"+uid+"/"+megid+"/"

        if not os.path.exists(set_dir):
            os.makedirs(set_dir)
        #convert string to arrays
        sets = sets.split(",")
        methods = methods.split(",")
        #only consider selected sets
        self.data_cluster = self.data[sets]
        #add megan file if available
        if megset:
            megset = megset.split(",")
            self.data_cluster = pd.concat([self.data_cluster, megdata.data[megset]], axis=1)
            self.data_cluster.fillna(0, inplace=True)
        #only consider the wanted taxonomic level
        #create dictionary for each taxid with the corresponding level
        idindex = self.data_cluster.index
        #fix for strings
        #idindex = map(str, map(int, self.data.index))

        if level != "raw":
            taxdict = {}
            with open('./metacluster/databases/nodes.dmp', 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter='\t')
                for row in csvreader:
                    taxdict[int(row[0])] = row[4]
            #iterate through the data chosen for clustering
            #drop all taxids not having the wanted level
            #INFO: check this part, takes a lot of time, faster implementation?
            for item in idindex:
                if item not in taxdict or taxdict[item] != level:
                    self.data_cluster = self.data_cluster.drop(item)

        idindex = self.data_cluster.index
        
        #TODO Warning if only zeros left after level filtering!
        if norm == "norm":
            #normalize
            self.data_cluster = self.data_cluster.div(self.data_cluster.sum(axis=0), axis=1).multiply(100000)
        elif norm == "subsample":
            #subsampling 50 tries, size 25% of set
            #parameters
            samplings = 50
            percentage = 25
            #calculation
            amount = len(self.data_cluster.index)/100*percentage
            interim = self.data_cluster.loc[np.random.choice(self.data_cluster.index, amount, replace=False)].sum(axis=0)
            for i in xrange(0, samplings-1):
                interim += self.data_cluster.loc[np.random.choice(self.data_cluster.index, amount, replace=False)].sum(axis=0)
            self.data_cluster = self.data_cluster.div(interim.div(samplings), axis=1).multiply(100000)

        if fpt:
            #remove unsiginificant ones / false positives to a certain threshold
            #sum up
            data_sum = self.data_cluster.sum(axis=1)
            #sort
            data_sum.sort(ascending=False)
            #get absolute value
            completesum = data_sum.sum()
            #get percentage for each and drop at x percent
            count = 0.0
            for key, item in data_sum.iteritems():
                if float(count/completesum*100) > float(fpt):
                    self.data_cluster = self.data_cluster.drop(key)
                else:
                    count += item

        idindex = self.data_cluster.index
        #fix for strings
        #idindex = map(str, map(int, self.data.index))
        if taxtrans:
            taxtransdict = {}
            with open('./metacluster/databases/names.dmp', 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter='\t')
                for row in csvreader:
                    if row[6] == "scientific name":
                        taxtransdict[int(row[0])] = row[2]

            newindex = []
            for item in idindex:
                if item in taxtransdict:
                    newindex.append(taxtransdict[item])
                else:
                    newindex.append(item)
            self.data_cluster.index = newindex
        #group same rows
        self.data_cluster = self.data_cluster.groupby(self.data_cluster.index).sum()
        #print self.data_cluster

        #get most abundant taxa information for plots
        i = 0
        first_five_tax = {}
        for name in self.data_cluster.T.index:
            first_five = self.data_cluster[[name]].sort(columns=name, ascending=False).head(5).index.values.tolist()
            first_five_tax[name] = first_five
            i += 1
        self.plottax = json.dumps(first_five_tax)
        
        self.data_cluster = self.data_cluster.dropna(axis=1, how="all")
        #call the selected methods
        #TODO change to dynamic/temporary files so multiple users can use it at the same time
        # currently the files are overwritten
        if "pca" in methods:
            if megset:
                megdata.pca = self.do_pca()
                megdata.clustering["pca"] = True
            else:
                self.pca = self.do_pca()
                self.clustering["pca"] = True
        else:
            if megset:
                megdata.pca = False
            self.pca = False
            self.clustering["pca"] = False

        if "pcoa" in methods:
            if megset:
                megdata.pcoa = self.do_pcoa()
                megdata.clustering["pcoa"] = True
            else:
                self.pcoa = self.do_pcoa()
                self.clustering["pcoa"] = True
        else:
            if megset:
                megdata.pcoa = False
            self.pcoa = False
            self.clustering["pcoa"] = False

        if "kmeans" in methods:
            if megset:
                megdata.kmeans = self.do_kmeans(int(k), int(kinit))
                megdata.clustering["kmeans"] = True
            else:
                self.kmeans = self.do_kmeans(int(k), int(kinit))
                self.clustering["kmeans"] = True
        else:
            if megset:
                megdata.kmeans = False
            self.kmeans = False
            self.clustering["kmeans"] = False

        if "tw" in methods:
            self.do_hierachrical(set_dir)
            if megset:
                megdata.clustering["hier"] = True
            else:
                self.clustering["hier"] = True
        else:
            self.clustering["hier"] = False

        if "pwl" in methods:
            self.jpowerlaw = get_power_law(self.data, sets)
            if megset:
                megdata.clustering["pwl"] = True
            else:
                self.clustering["pwl"] = True
        else:
            self.jpowerlaw = False
            self.clustering["pwl"] = False

        if kdevdata:
            kdevdata = kdevdata.split(",")
            if len(kdevdata) != 2:
                kdevdata.pop(0)
                kdevdata.pop(0)
                kdevdata.insert(0,",")
            self.kdev = self.do_kdev(kdevdata, fpt, norm, level)
            self.clustering["kdev"] = True
        else:
            self.kdev = False
            self.clustering["kdev"] = False

    def do_kdev(self, kdevdata, fpt, norm, level):
        feature = kdevdata[1]
        splitchar = kdevdata[0]
        graph = ""

        itemlist = []
        for item in self.trans[feature]:
            itemlist.append(item)
        unique_list = list((set(itemlist)))
        featurelist = {}
        for item in unique_list:
            featurelist[item] = self.trans[self.trans[feature].isin([item])].index.tolist()

        kdev_data = self.data
        
        idindex = kdev_data.index

        if level != "raw":
            taxdict = {}
            with open('./metacluster/databases/nodes.dmp', 'rb') as csvfile:
                csvreader = csv.reader(csvfile, delimiter='\t')
                for row in csvreader:
                    taxdict[int(row[0])] = row[4]
            for item in idindex:
                if item not in taxdict or taxdict[item] != level:        
                    kdev_data = kdev_data.drop(item)

        graph += '\'digraph G { graph [center=1 rankdir=LR dpi=100 ] edge [dir=none] node [width=0.3 height=0.3 label=""]\t'

        cluster_details = {}
        label_list = []

        

        for key, item in featurelist.iteritems():
            availelements = []
            for name in item:
                if name in kdev_data.columns.tolist():
                    availelements.append(name)

            data_cluster = kdev_data[availelements]

            if norm:
                #normalize
                data_cluster = data_cluster.div(data_cluster.sum(axis=0), axis=1).multiply(100000)
            
            if fpt:
                #remove unsiginificant ones / false positives to a certain threshold
                #sum up
                data_sum = data_cluster.sum(axis=1)
                #sort
                data_sum.sort(ascending=False)
                #get absolute value
                completesum = data_sum.sum()
                #get percentage for each and drop at x percent
                count = 0.0
                for fkey, item in data_sum.iteritems():
                    if float(count/completesum*100) > float(fpt):
                        data_cluster = data_cluster.drop(fkey)
                    else:
                        count += item

            data_cluster = data_cluster.dropna(axis=1, how="all")

            initscore = -1
            #begin k=2, increase till score drops
            for k in xrange(2,50):
                #TODO: make n_init variable!
                k_means = cluster.KMeans(n_clusters=k,n_init=100)
                k_means.fit(data_cluster.T)
                labels = k_means.labels_
                score = metrics.silhouette_score(data_cluster.T, labels, metric='euclidean')
                if score > initscore:
                    initscore = score
                    old_labels = labels
                else:
                    break


            #TODO: could be done faster
            samplelabels = {}
            i = 0
            for name in data_cluster.T.index.tolist():
                if str(old_labels[i]) not in samplelabels:
                    samplelabels[str(old_labels[i])] = []
                    samplelabels[str(old_labels[i])].append(name)
                else:
                    samplelabels[str(old_labels[i])].append(name)
                i += 1

            graph +=  'subgraph cluster_'+str(int(key))+' { node [shape=circle] label = "'+feature+' : '+str(int(key))+'";\t'
            for cid in samplelabels:
                graph +=  str(int(key))+str(cid)+"\t"
            cluster_details[str(int(key))] = samplelabels
            label_list.append(str(int(key)))
            graph +=  '}\t'
            
        cluster_details_new = {}
        #restructure cluster_details for easier access later on
        for ekey, entry in cluster_details.iteritems():
            cluster_details_new[ekey] = {}
            for ikey, item in entry.iteritems():
                for name in item:
                    cluster_details_new[ekey][name.split(splitchar)[0]] = ikey

        #define list of different colors to use for cluster coloring
        colors = ["#9669FE", "#23819C", "#01F33E", "#F7DE00", "#FF800D", "#B96F6F", "#F70000", "#B300B3", "#4FBDDD", "#0000CE", "#74BAAC"]
        color_table = {}

        #color based on first cluster:
        color = 0
        first_clusters = cluster_details[sorted(cluster_details.keys())[0]]
        for clusterid, name in first_clusters.iteritems():
            for item in name:
                color_table[item.split(splitchar)[0]] = colors[color]
            color += 1


        #use label_list to preserve order, as dict always sorts
        first = True
        edge_dict = {}
        for label in label_list:
            if not first:
                for key, clusterid in cluster_details_new[label].iteritems():
                    if key in prev_entry:
                        edge = prev_ekey+prev_entry[key]+'->'+label+clusterid
                        if edge not in edge_dict:
                            if key in color_table:
                                edge_dict[edge] = color_table[key]
                            else:
                                edge_dict[edge] = "#CC0033"
                        else:
                            if key in color_table:
                                edge_dict[edge] += ":"+color_table[key]
                            else:
                                edge_dict[edge] = ":#CC0033"
            prev_entry = cluster_details_new[label]
            prev_ekey = label
            first = False

        color_count = 0
        for name, details in edge_dict.iteritems():
            graph += '{ edge [color="'
            graph += details
            graph +=  '"]\t'
            graph +=  name+"\t"
            graph +=  '}\t'

        graph +=  '}\''

        return graph

    def do_pca(self):
        #init and do the pca with 3 components
        pca = decomposition.PCA(n_components=3)
        pca.fit(self.data_cluster.T)
        X = pca.transform(self.data_cluster.T)
        #get point labels and add to result of pca
        i = 0
        Xlist = X.tolist()
        for name in self.data_cluster.T.index:
            Xlist[i] = Xlist[i]+[name]
            i += 1
        return json.dumps(Xlist)

    def do_pcoa(self, method="braycurtis"):
        #default method: braycurtis, alternative: jaccard
        distmatrix = pw_distances(self.data_cluster.T, self.data_cluster.columns.tolist(), method)
        pc = PCoA(distmatrix).scores()
        all_pcs = pc.site.T
        X = all_pcs[0:3].T
        #get point labels and add to result of pca
        i = 0
        Xlist = X.tolist()
        for name in self.data_cluster.T.index:
            Xlist[i] = Xlist[i]+[name]
            i += 1
        return json.dumps(Xlist)

    def pca_kmeans(self, pcadata, k=3, kinit=50):
        #get pcadata string back to array
        pcadata = pcadata.split(",")
        pcadata_kmeans = []
        for i in range(1, len(pcadata), 2):
            pcadata_kmeans.append([float(pcadata[i-1]), float(pcadata[i])])
        #do k-means on data
        k_meanspca = cluster.KMeans(n_clusters=int(k), n_init=kinit)
        k_meanspca.fit(pcadata_kmeans)
        pcalabels = k_meanspca.labels_
        #return calculated labels
        return json.dumps(pcalabels.tolist())

    def do_kmeans(self, k=3, kinit=50):
        k_means = cluster.KMeans(n_clusters=int(k), n_init=kinit)
        k_means.fit(self.data_cluster.T)
        labels = k_means.labels_
        
        samplelabels = {}
        i = 0
        #add sample name to labels
        for name in self.data_cluster.T.index.tolist():
            if str(labels[i]) not in samplelabels:
                samplelabels[str(labels[i])] = []
                samplelabels[str(labels[i])].append(name)
            else:
                samplelabels[str(labels[i])].append(name)
            i += 1

        return json.dumps(samplelabels)

    def do_hierachrical(self, set_dir):
        zscore_df = self.data_cluster.T.apply(lambda row: (row-row.mean(axis=1))/row.std(axis=1), axis=1)
        
        #cluster y
        distVec_sample = dist.pdist(zscore_df)
        linkage_sample = shi.linkage(distVec_sample, method='complete')
        heatmapOrder_sample = shi.leaves_list(linkage_sample)

        #cluster x
        distVec_taxon = dist.pdist(zscore_df.T)
        linkage_taxon = shi.linkage(distVec_taxon)
        heatmapOrder_taxon = shi.leaves_list(linkage_taxon)
        #order
        ordered_zscore = zscore_df.ix[heatmapOrder_sample, heatmapOrder_taxon]
        #get ordered labels
        column_labels = list(ordered_zscore.columns.tolist())
        row_labels = list(ordered_zscore.index.values.tolist())

        #set size semi-dynamically
        sample_num = len(zscore_df.index)
        if sample_num <= 20:
            pic_size = 10
        elif sample_num <= 40:
            pic_size = 15
        elif sample_num <= 80:
            pic_size = 20
        else:
            pic_size = 25
        #plot it
        fig = plt.figure(figsize=(19, pic_size))

        #dendro1 - sample
        ax1 = fig.add_axes([0.05,0.1,0.2,0.6], frameon=0)
        ax1.set_yticks(np.arange(len(ordered_zscore.index))+0.5, minor=False)
        ax1.set_yticklabels(row_labels, minor=False)
        ax1.set_xticks([])
        ax1.set_ylabel("Samples")
        d1 = shi.dendrogram(linkage_sample, orientation="right", no_labels=True, color_threshold=0)

        #dendro2 - taxon
        ax2 = fig.add_axes([0.3,0.71,0.6,0.2], frameon=0)
        ax2.set_yticks([])
        ax2.set_xticks([])
        ax2.set_xlabel("Taxa", va='top')
        ax2.xaxis.set_label_position('top')
        d2 = shi.dendrogram(linkage_taxon, no_labels=True, color_threshold=0)

        ax = fig.add_axes([0.3,0.1,0.6,0.6])
        plt.xlim(0, len(ordered_zscore.columns))
        plt.ylim(0, len(ordered_zscore.index))
        im = plt.pcolor(ordered_zscore, cmap=plt.cm.Blues)
        ax.set_xticks(np.arange(len(ordered_zscore.columns)), minor=False)
        ax.set_yticks(np.arange(len(ordered_zscore.index))+0.5, minor=False)
        ax.set_yticklabels(row_labels, minor=False)
        xlabel = ax.set_xticklabels(column_labels, minor=False, rotation=70)

        axcolor = fig.add_axes([0.91,0.1,0.02,0.6])
        colb = plt.colorbar(im, cax=axcolor)
        colb.set_label("Z-Score")
        fig.savefig("%sheatmap.png" % set_dir, bbox_inches='tight')

    def group_dataset(self, feature, transset):
        itemlist = []
        for item in transset[feature]:
            itemlist.append(item)
        unique_list = list((set(itemlist)))
        featurelist = {}
        for item in unique_list:
            featurelist[item] = transset[transset[feature].isin([item])].index.tolist()
        return json.dumps(featurelist)

class MeganFile(object):
    def __init__(self, megfile):
        if isinstance(megfile, cherrypy._cpreqbody.Part):
            self.clustering = {}
            self.clustering["kdev"] = False
            self.clustering["kmeans"] = False
            self.clustering["pcoa"] = False
            self.clustering["pwl"] = False
            self.clustering["pca"] = False
            self.clustering["hier"] = False
            #create new tmp directory
            tmp_dir = tempfile.mkdtemp(dir="./tmp/")
            if megfile.filename.split(".")[1] == "zip":
                #extract all files in temp dir
                with zipfile.ZipFile(megfile.file) as myzip:
                    myzip.extractall(tmp_dir)
                merged_csv = mergeMeganFolder(tmp_dir)
            elif megfile.filename.split(".")[1].lower() == "megan":
                with open(tmp_dir+"/"+megfile.filename.split(".")[0]+".megan", 'w') as myFile:
                    myFile.write(megfile.file.read())
                merged_csv = mergeMeganFolder(tmp_dir)
            else:
                merged_csv = megfile.file
            #save csv as meganfile object
            self.data = pd.read_csv(merged_csv, index_col = 0, delimiter="\t", dtype=np.float)
            #fix for string problem
            if type(self.data.index[0]) == np.float64:
                self.data.index = map(str, map(int, self.data.index))
                self.data.index = map(int, map(str, self.data.index))
            #strip leading whitespaces in names
            self.data.columns = self.data.columns.map(lambda x: x.strip())
            #sort for better viewing later on
            self.data = self.data.sort(axis=1)
            #delete tmp folder and all data in it
            shutil.rmtree(tmp_dir)
            #drop unassigned data if present (-1)
            if -1 in self.data.index:
                self.data = self.data.drop(-1)
            if -2 in self.data.index:
                self.data = self.data.drop(-2)
            if -3 in self.data.index:
                self.data = self.data.drop(-3)
            #fix for string
            if "-1" in self.data.index:
                self.data = self.data.drop("-1")
            if "-2" in self.data.index:
                self.data = self.data.drop("-2")
            if "-3" in self.data.index:
                self.data = self.data.drop("-3")
        else:
            self.clustering = {}
            self.clustering["kdev"] = False
            self.clustering["kmeans"] = False
            self.clustering["pcoa"] = False
            self.clustering["pwl"] = False
            self.clustering["pca"] = False
            
            self.data = megfile.data

        #TODO: move to clustering
        #get powerlaw
        self.jpowerlaw = get_power_law(self.data)

#############
# Functions #
#############

def mergeMeganFolder(tmpdir):
    #merge folder
    sample_list = []
    if tmpdir:
        tax_dict = {}
        #loop through folder
        for files in os.walk(tmpdir):
            all_samples = 0
            for meg_file in files[2]:
                with open(files[0]+"/"+meg_file, "r") as infile:
                    samples = 0
                    for line in infile:
                        if line.startswith('@'):
                            parts = line.strip().split("\t")
                            if parts[0] == "@Names":
                                sample_list += parts[1:]
                                samples = len(parts[1:])
                        if line.startswith('TAX'):
                            parts = line.strip().split("\t")
                            if len(parts)-2 != samples:
                                for i in xrange(len(parts)-2, samples):
                                    parts.append('0')
                            if parts[1] in tax_dict:
                                tax_dict[parts[1]] = tax_dict[parts[1]]+[ int(x) for x in parts[2:] ]
                            else:
                                tax_dict[parts[1]] = [0]*all_samples+[ int(x) for x in parts[2:] ]
                num_samples = len(parts[2:])
                all_samples += num_samples
                #replace missing values
                for key, value in tax_dict.iteritems():
                    if len(value) != all_samples:
                        tax_dict[key] = tax_dict[key]+[0]*num_samples
        #write output file
        outfile = tmpdir+"/output.csv"
        with open(outfile, 'wb') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter='\t')
            #TODO naming! (temp filename)
            csvwriter.writerow(["TaxID"]+sample_list)
            for key, value in tax_dict.iteritems():
                csvwriter.writerow([key] + value)
    return outfile

def get_power_law(data, sets=False):
    #only consider selected sets
    if sets:
        setdata = data[sets]
    else:
        setdata = data
    jpowerlaw = {}
    jraw = json.loads(setdata.to_json())
    for name in jraw.keys():
        jdata = powerlaw.Fit(setdata[name], xmin=1.0, discrete=True)
        x, y = jdata.ccdf()
        jpowerlaw[name] = []
        for i in range(0,len(x)):
            jpowerlaw[name].append([x[i], y[i]])
    return json.dumps(jpowerlaw)

def isInt(s):
    try: 
        int(s)
        return True
    except ValueError:
        return False
