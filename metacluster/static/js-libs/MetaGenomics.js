/**
 * @author Jens Rauch
 */

THREE.MetaGenomics = function(translation, dataset) {
    
    // Attributes

    //do not change, internal stuff
    var _this = this;
    this.translation = translation;
    this.dataset = dataset;
    this.objects = [];

    //config part, edit these if needed
    //set maximal range and normalize all points to this value
    this.axesrange = 30;
    //x, y, z
    this.axescolor = ["red", "green", "blue"]
    //default color
    this.defcol = 0xCC3300;

    // Methods

    // initialize axes based on length/width of the dataset
    this.initAxes = function() {
        // create the three main axes
        group = new THREE.Object3D();
        lineGeo = new THREE.Geometry();
        // add points to create line from
        lineGeo.vertices.push( vec(-_this.axesrange, 0, 0), vec(_this.axesrange, 0, 0) );
        // set color and material
        line = new THREE.Line(lineGeo, new THREE.LineBasicMaterial( {color: _this.axescolor[0], lineWidth: 1} ));
        line.type = THREE.Lines;
        // add to group
        group.add(line);

        lineGeo = new THREE.Geometry();
        lineGeo.vertices.push( vec(0, -_this.axesrange, 0), vec(0, _this.axesrange, 0) );
        line = new THREE.Line(lineGeo, new THREE.LineBasicMaterial( {color: _this.axescolor[1], lineWidth: 1} ));
        line.type = THREE.Lines;
        group.add(line);

        lineGeo = new THREE.Geometry();
        lineGeo.vertices.push( vec(0, 0, -_this.axesrange), vec(0, 0, _this.axesrange) );
        line = new THREE.Line(lineGeo, new THREE.LineBasicMaterial( {color: _this.axescolor[2], lineWidth: 1} ));
        line.type = THREE.Lines;
        group.add(line);

        return group;
    };

    //function to show or update the viewed data, based on the axis given to the method
    this.showData = function(xax, yax, zax) {
        _this.objects = [];
        group = new THREE.Object3D();

        //get maximum for normalization
        //x axis
        xmax = 0;
        for(key in _this.translation[Object.keys(_this.translation)[0]]) {
            item = parseFloat(_this.translation[xax][key]);
            if (item > xmax) xmax = item;
        }
        //y axis
        ymax = 0;
        for(key in _this.translation[Object.keys(_this.translation)[0]]) {
            item = parseFloat(_this.translation[yax][key]);
            if (item > ymax) ymax = item;
        }
        //z axis
        zmax = 0;
        for(key in _this.translation[Object.keys(_this.translation)[0]]) {
            item = parseFloat(_this.translation[zax][key]);
            if (item > zmax) zmax = item;
        }

        //create point for each entry in dataset
        for(key in _this.translation[Object.keys(_this.translation)[0]]) {
            //create point for currently selected axes
            currentpoint = new THREE.Mesh( new THREE.SphereGeometry(0.3,0.3,0.3), new THREE.MeshBasicMaterial({ color: _this.defcol }) );
            //get position for each point
            //xaxis
            num = parseFloat(_this.translation[xax][key]);
            //check if it is a number
            //TODO: Think about a more general method to convert pure strings to numbers
            if(isNaN(num)) {
                //gender special case
                if(xax == "Gender") {
                    if(_this.translation[xax][key] == "f") {
                        xcoord = -10
                    }
                    else if(_this.translation[xax][key] == "m") {
                        xcoord = 10
                    }
                    else {
                        xcoord = 0
                    }
                }
            }
            else {
                xcoord = num/xmax*_this.axesrange;
            }
            //yaxis
            num = parseFloat(_this.translation[yax][key]);
            if(isNaN(num)) {
                //gender special case
                if(yax == "Gender") {
                    if(_this.translation[yax][key] == "f") {
                        ycoord = -10
                    }
                    else if(_this.translation[yax][key] == "m") {
                        ycoord = 10
                    }
                    else {
                        ycoord = 0
                    }
                }
            }
            else {
                ycoord = num/ymax*_this.axesrange;
            }
            //zaxis
            num = parseFloat(_this.translation[zax][key]);
            if(isNaN(num)) {
                //gender special case
                if(zax == "Gender") {
                    if(_this.translation[zax][key] == "f") {
                        zcoord = -10
                    }
                    else if(_this.translation[zax][key] == "m") {
                        zcoord = 10
                    }
                    else {
                        zcoord = 0
                    }
                }
            }
            else {
                zcoord = num/zmax*_this.axesrange;
            }
            //change position of current point
            currentpoint.position = new THREE.Vector3(xcoord, ycoord, zcoord);
            //name the objects for easier access later on
            currentpoint.name = key;
            //add point to group
            group.add(currentpoint);
            //push each point into the objects array to check later on for intersection with the raycaster
            _this.objects.push(currentpoint);
        }
        return group;
    }

    //change the coloring of the points according to one of the features
    this.changeColor = function(points, feature) {
        //allowed values: default or one of the features
        if(feature == "default") {
            for(item in points) {
                //set color for all points back to the default value
                item.material.color.setHex(_this.defcol);
            }
        }
        else {
            //coloring based on bins
            //get maximal value
            max = 0;
            for(key in _this.translation[Object.keys(_this.translation)[0]]) {
                item = parseFloat(_this.translation[feature][key]);
                if (item > max) max = item;
            }
            //calculate bins (maximal 20)


            for(key in _this.translation[Object.keys(_this.translation)[0]]) {
                

                num = parseFloat(_this.translation[feature][key]);
            }
        }
        //TODO: add coloring based on clustering?
    }

    // double-click event
    //TODO: variable dialog class name?
    this.DbCl = function( event ) {
        //get mouse position
        var mouse = new THREE.Vector2();
        var selected = [];
        event.preventDefault();
        mouse = getMouseInContainer(event.clientX, event.clientY);
        var vector = new THREE.Vector3( mouse.x, mouse.y, 0.5 );
        projector.unprojectVector( vector, camera );
        //check for intersection
        var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );
        var intersects = ray.intersectObjects( _this.objects );
        if ( intersects.length > 0 ) {
            //close old dialog
            $("#dialog").dialog('close');
            //get selected points
            selected = selected.concat(intersects);
            //get name of closest point to the curser
            pointname = selected[0].object.name;

            // construct popup in HTML
            list="Metagenomic Information:<br>";
            list+="------------------------";
            list+="<table><tr><td>Taxonomy ID:</td><td>Count:</td></tr>";
            //TODO: change to variable pointname!!
            //NOTE: this needs ALL data in the uploaded dataset, maybe get a way around
            //and just show data from the translation table also avaiblable in the dataset?
            for(key in _this.dataset["AS43_3"]) {
                list=list+"<tr><td>"+key+"</td><td>"+_this.dataset["AS43_3"][key]+"</td></tr>";
            }
            list+="</table>";
            list+="------------------------";

            $("#dialog").html(list);

            // show pop-up and center camera on specific position
            $("#dialog").dialog('open');
            $("#dialog").dialog( "option", "title", "Content" );
        }
    };

    // Functions

    // helper function, reduce code, returns point
    vec = function(x,y,z){ 
        return new THREE.Vector3(x,y,z); 
    };

    // get pos relative to container
    getMouseInContainer = function(clientX, clientY) {
        screen = { width: window.innerWidth, height: window.innerHeight, offsetLeft: 0, offsetTop: 0 };
        if (!container) {
            return new THREE.Vector2(
                (clientX - screen.width * 0.5 - screen.offsetLeft) / (screen.width * 0.5),
                (screen.height * 0.5 + screen.offsetTop - clientY) / (screen.height * 0.5));
        }
        var currentElement = container;
        var totalOffsetX = currentElement.offsetLeft - currentElement.scrollLeft;
        var totalOffsetY = currentElement.offsetTop - currentElement.scrollTop;
        var containerY   = 0;
        var containerX   = 0;
        var canvasY      = 0;
        
        while (currentElement = currentElement.offsetParent) {
            totalOffsetX += currentElement.offsetLeft - $(document).scrollLeft();
            totalOffsetY += currentElement.offsetTop - $(document).scrollTop();
        }
        
        containerX = clientX - totalOffsetX;
        containerY = clientY - totalOffsetY;
        return new THREE.Vector2(
            (containerX - container.offsetWidth * 0.5) / (container.offsetWidth * .5),
            (container.offsetHeight * 0.5 - containerY) / (container.offsetHeight * .5));
    };

};