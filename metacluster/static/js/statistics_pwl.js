ShowStats.plot_pwl = function() {
    //init
    var plotpwl = [];
    for (var key in jpl) {
        plotpwl.push(jpl[key]);
    }
    if(jplmeg != false) {
        for (var key in jplmeg) {
            plotpwl.push(jplmeg[key]);
        }
    }


    var pwloptions = {
        //show legend
        legend: {
            show: true,
            renderer: $.jqplot.EnhancedLegendRenderer,
            rendererOptions: {
                numberRows: 15
            },
            placement : "outside"
        },
        seriesDefaults: {
            pointLabels:{ show:false }
        },
        //use log axis
        axes:{
            yaxis:{
                label:'Proportion of number of taxa with count >= #Taxa',
                renderer: $.jqplot.LogAxisRenderer,
                tickOptions:{
                    labelPosition: 'middle',
                    angle:-30
                },
                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions:{
                    fontFamily:'Helvetica',
                    fontSize: '14pt'
                }
            },
            xaxis:{
                label:'#Taxa',
                renderer: $.jqplot.LogAxisRenderer,
                tickOptions:{
                    angle: -30
                },
                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                labelOptions:{
                    fontFamily:'Helvetica',
                    fontSize: '14pt'
                },
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
            }
        }
    };

    $(document).ready(function(){
        //push series
        series = []
        if(jplmeg != false) {
            //add labels and remove dots as marker
            for (var key in jpl) {
                series.push({showMarker:false, showLabel:false, label:key});
            }
            for (var key in jplmeg) {
                series.push({showMarker:false, showLabel:false, label:key});
            }
        }
        else {
            //add labels and remove dots as marker
            for (var key in jpl) {
                series.push({showMarker:false, showLabel:false, label:key});
            }
        }
        pwloptions["series"] = series;

        $("#savepwl").button();


        //plot
        $.jqplot('powerlaw', plotpwl, pwloptions);



        //save
        //TODO: filename?
        $("#savepwl").click(function() {
            $('#powerlaw').jqplotSaveImage();
        });

    });
}