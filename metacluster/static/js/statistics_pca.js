ShowStats.plot_pca = function(pca_data) {

    var _this = this;
    this.pcadata = pca_data;
    this.current_pcs = [0,1];
    this.pcaplot;
    this.pcaoptions = {
        //_this.pcaplot.legend.show = true;
        legend: {
            show: false
        },
        title:'PCA result',
        seriesDefaults: {    
            showLine:false,
            pointLabels:{ show:true, location:'s', ypadding:3 }
        },
        axes: {
            xaxis: {
                label:'1st Principal Component',
                tickOptions:{
                    angle: -30
                },
                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                labelOptions:{
                    fontFamily:'Helvetica',
                    fontSize: '14pt'
                },
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
            },
            yaxis: {
                label:'2nd Principal Component',
                tickOptions:{
                    labelPosition: 'middle',
                    angle:-30
                },
                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions:{
                    fontFamily:'Helvetica',
                    fontSize: '14pt'
                }
            }
        }
    };

    $(document).ready(function(){
        $("#pcalabel").button();
        $("#pcaclust").button();
        $("#pcacol").button();
        $(".pcacol").select2();
        $(".choosepc_pca").select2();
        $("#savepca").button();
        $( "#kspinner_pca" ).spinner({min: 0});
        $( "#kspinner_pca" ).spinner("value", 3);
        //another bugfix for firefox to "not remember" dropdowns
        $("#y.choosepc_pca option[value='2']").prop('selected', 'selected');

        //plot
        _this.pcaplot = $.jqplot('pcaplot', [getPCs(_this.pcadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcaoptions);
        plottooltip('#pcaplot', '#pcatooltip');


        //show/hide labels
        $("#pcalabel").change(function() {
            if($(this).is(":checked")) {
                _this.pcaoptions.seriesDefaults.pointLabels.show = false;
            }
            else {
                _this.pcaoptions.seriesDefaults.pointLabels.show = true;
            }
            _this.pcaplot.destroy();
            _this.pcaplot = $.jqplot('pcaplot', [getPCs(_this.pcadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcaoptions);
            $(".pcacol").trigger("change");
            //kmeans?
        });


        //choose PC axes
        $(".choosepc_pca").change(function() {
            if($(this).attr("id") == "x") {
                _this.current_pcs[0] = parseInt($(this).select2("val")-1);
                switch(parseInt($(this).select2("val"))){
                    case 1:
                        _this.pcaoptions.axes.xaxis.label = "1st Principal Component";
                        break;
                    case 2:
                        _this.pcaoptions.axes.xaxis.label = "2nd Principal Component";
                        break;
                    case 3:
                        _this.pcaoptions.axes.xaxis.label = "3rd Principal Component";
                        break;
                }
                
            }
            else if($(this).attr("id") == "y") {
                _this.current_pcs[1] = parseInt($(this).select2("val")-1);
                switch(parseInt($(this).select2("val"))){
                    case 1:
                        _this.pcaoptions.axes.yaxis.label = "1st Principal Component";
                        break;
                    case 2:
                        _this.pcaoptions.axes.yaxis.label = "2nd Principal Component";
                        break;
                    case 3:
                        _this.pcaoptions.axes.yaxis.label = "3rd Principal Component";
                        break;
                }
            }
            _this.pcaplot.destroy();
            _this.pcaplot = $.jqplot('pcaplot', [getPCs(_this.pcadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcaoptions);
            $(".pcacol").trigger("change");
        });


        //cluster PCA with k-means
        //do k-means
        $("#pcaclust").click(function() {
            //change coloring select entry
            $(".pcacol").select2('val', 'kmeans');

            k = $( "#kspinner_pca" ).spinner("value");
            //remove name from data in order to do k-means
            pcadata_removed = [];
            for(key in _this.pcadata){
                pcadata_removed.push([_this.pcadata[key][_this.current_pcs[0]], _this.pcadata[key][_this.current_pcs[1]]]);
            }
            //do post request to use python function for k-means
            $.ajax({
                type: "POST",
                data: {
                    pcadata: pcadata_removed.toString(),
                    k: k
                },
                success: function(data) {
                    var klabels = data;
                    plotpcak = [];
                    series = [];
                    for(var i=0; i<k; i++) {
                        plotpcak[i] = [];
                    }
                    for(key in _this.pcadata) {
                        plotpcak[klabels[key]].push(_this.pcadata[key]);
                        series.push({label: "Cluster "+(parseInt(key)+1)});
                    }
                    _this.pcaplot.destroy();
                    _this.pcaoptions["series"] = series;
                    _this.pcaoptions.legend = {
                        show: true,
                        renderer: $.jqplot.EnhancedLegendRenderer,
                        rendererOptions: {
                            numberRows: 20
                        },
                        placement : "outside"
                    };
                    _this.pcaplot = $.jqplot('pcaplot', getPCs_col(plotpcak, _this.current_pcs[0], _this.current_pcs[1]), _this.pcaoptions);
                },
                dataType: "json"
            });
        });


        //save PCA
        $("#savepca").click(function() {
            $('#pcaplot').jqplotSaveImage();
        });


        //toggle pca cluster coloring based on k-means data
        $("#pcacol").change(function() {
            if($(this).is(":checked")) {
                //change coloring select entry
                $(".pcacol option[value='kmeans']").prop('selected', 'selected');
                var plotpcak = [];
                series = []
                for(key in kmeans) {
                    var kseries = [];
                    //TODO: ugly and slow code (better way?)
                    for( i in kmeans[key]) {
                        for( j in _this.pcadata) {
                            if(kmeans[key][i] == _this.pcadata[j][3]) {
                                kseries.push(_this.pcadata[j]);
                                continue;
                            }
                        }
                    }
                    plotpcak.push(kseries);
                    series.push({label: "Cluster "+(parseInt(key)+1)});
                }
                _this.pcaplot.destroy();
                _this.pcaoptions["series"] = series;
                _this.pcaoptions.legend = {
                    show: true,
                    renderer: $.jqplot.EnhancedLegendRenderer,
                    rendererOptions: {
                        numberRows: 20
                    },
                    placement : "outside"
                };
                _this.pcaplot = $.jqplot('pcaplot', getPCs_col(plotpcak, _this.current_pcs[0], _this.current_pcs[1]), _this.pcaoptions);
            }
            else {
                //change coloring select entry
                $(".pcacol option[value='nocol']").prop('selected', 'selected');
                _this.pcaplot.destroy();
                _this.pcaoptions.legend = { show: false };
                _this.pcaplot = $.jqplot('pcaplot', [getPCs(_this.pcadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcaoptions);
            }
        });


        //change color in PCA by dropdown
        $(".pcacol").change(function() {
            if($(".pcacol").select2("val") == "nocol") {
                //reset coloring
                _this.pcaplot.destroy();
                _this.pcaoptions.legend = { show: false };
                _this.pcaplot = $.jqplot('pcaplot', [getPCs(_this.pcadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcaoptions);
            }
            else {
                $.ajax({
                    type: "POST",
                    data: {
                        feature: $(".pcacol").select2("val")
                    },
                    success: function(data) {
                        var features = data;
                        //change col
                        var plotpcak = [];
                        series = [];
                        foundFeatures = [];
                        for(key in features) {
                            var fseries = [];
                            //TODO: ugly and slow code (better way?)
                            for( i in features[key]) {
                                for( j in _this.pcadata) {
                                    if(features[key][i] == _this.pcadata[j][3]) {
                                        foundFeatures.push(_this.pcadata[j][3]);
                                        fseries.push(_this.pcadata[j]);
                                        continue;
                                    }
                                }
                            }
                            if(typeof fseries != "undefined" && fseries != null && fseries.length > 0) {
                                plotpcak.push(fseries);
                                if(!isNaN(key)) {
                                    if(parseInt(key) == 0) {
                                        key = "0";
                                    }
                                    else {
                                        key = Math.round(key*Math.pow(10,3))/Math.pow(10,3);
                                    }
                                }
                                series.push({label: key});
                            }
                        }
                        //add all points not in features
                        var fseries = [];
                        for(item in _this.pcadata) {
                            if(foundFeatures.indexOf(_this.pcadata[item][3]) < 0) {
                                fseries.push(_this.pcadata[item]);
                            }
                        }
                        plotpcak.push(fseries);
                        series.push({label: "No metadata"});
                        _this.pcaplot.destroy();
                        _this.pcaoptions["series"] = series;
                        _this.pcaoptions.legend = {
                            show: true,
                            renderer: $.jqplot.EnhancedLegendRenderer,
                            rendererOptions: {
                                numberRows: 20
                            },
                            placement : "outside"
                        };
                        _this.pcaplot = $.jqplot('pcaplot', getPCs_col(plotpcak, _this.current_pcs[0], _this.current_pcs[1]), _this.pcaoptions);
                    },
                    dataType: "json"
                });
            }
        });
    });

}