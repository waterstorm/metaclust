ShowStats.plot_pcoa = function(pcoa_data) {

    var _this = this;
    this.pcoadata = pcoa_data;
    this.current_pcs = [0,1];
    this.pcoaplot;
    this.pcoaoptions = {
        //pcaplot.legend.show = true;
        legend: {
            show: false
        },
        title:'PCoA result',
        seriesDefaults: {    
            showLine:false,
            pointLabels:{ show:true, location:'s', ypadding:3 }
        },
        axes: {
            xaxis: {
                label:'1st Principal Coordinates',
                tickOptions:{
                    angle: -30
                },
                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                labelOptions:{
                    fontFamily:'Helvetica',
                    fontSize: '14pt'
                },
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer
            },
            yaxis: {
                label:'2nd Principal Coordinates',
                tickOptions:{
                    labelPosition: 'middle',
                    angle:-30
                },
                tickRenderer:$.jqplot.CanvasAxisTickRenderer,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions:{
                    fontFamily:'Helvetica',
                    fontSize: '14pt'
                }
            }
        }
    };

    $(document).ready(function(){
        $("#savepcoa").button();
        $("#pcoalabel").button();
        $("#pcoaclust").button();
        $("#pcoacol").button();
        $(".pcoacol").select2();
        $(".choosepc_pcoa").select2();
        $( "#kspinner_pcoa" ).spinner({min: 0});
        $( "#kspinner_pcoa" ).spinner("value", 3);

        //another bugfix for firefox to "not remember" dropdowns
        $("#y.choosepc_pcoa option[value='2']").prop('selected', 'selected');

        //plot
        _this.pcoaplot = $.jqplot('pcoaplot', [getPCs(_this.pcoadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcoaoptions);
        plottooltip('#pcoaplot', '#pcoatooltip');


        //show/hide labels
        $("#pcoalabel").change(function() {
            if($(this).is(":checked")) {
                _this.pcoaoptions.seriesDefaults.pointLabels.show = false;
            }
            else {
                _this.pcoaoptions.seriesDefaults.pointLabels.show = true;
            }
            _this.pcoaplot.destroy();
            _this.pcoaplot = $.jqplot('pcoaplot', [getPCs(_this.pcoadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcoaoptions);
            $(".pcoacol").trigger("change");
            //kmeans?
        });


        //choose PC axes
        $(".choosepc_pcoa").change(function() {
            if($(this).attr("id") == "x") {
                _this.current_pcs[0] = parseInt($(this).select2("val")-1);
                switch(parseInt($(this).select2("val"))){
                    case 1:
                        _this.pcoaoptions.axes.xaxis.label = "1st Principal Coordinates";
                        break;
                    case 2:
                        _this.pcoaoptions.axes.xaxis.label = "2nd Principal Coordinates";
                        break;
                    case 3:
                        _this.pcoaoptions.axes.xaxis.label = "3rd Principal Coordinates";
                        break;
                }
                
            }
            else if($(this).attr("id") == "y") {
                _this.current_pcs[1] = parseInt($(this).select2("val")-1);
                switch(parseInt($(this).select2("val"))){
                    case 1:
                        _this.pcoaoptions.axes.yaxis.label = "1st Principal Coordinates";
                        break;
                    case 2:
                        _this.pcoaoptions.axes.yaxis.label = "2nd Principal Coordinates";
                        break;
                    case 3:
                        _this.pcoaoptions.axes.yaxis.label = "3rd Principal Coordinates";
                        break;
                }
            }
            _this.pcoaplot.destroy();
            _this.pcoaplot = $.jqplot('pcoaplot', [getPCs(_this.pcoadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcoaoptions);
            $(".pcoacol").trigger("change");
        });


        //cluster pcoa with k-means
        //do k-means
        $("#pcoaclust").click(function() {
            //change coloring select entry
            $(".pcoacol").select2('val', 'kmeans');

            k = $( "#kspinner_pcoa" ).spinner("value");
            //remove name from data in order to do k-means
            pcoadata_removed = [];
            for(key in _this.pcoadata){
                pcoadata_removed.push([_this.pcoadata[key][_this.current_pcs[0]], _this.pcoadata[key][_this.current_pcs[1]]]);
            }
            //do post request to use python function for k-means
            $.ajax({
                type: "POST",
                data: {
                    pcadata: pcoadata_removed.toString(),
                    k: k
                },
                success: function(data) {
                    var klabels = data;
                    plotpcoak = [];
                    series = [];
                    for(var i=0; i<k; i++) {
                        plotpcoak[i] = [];
                    }
                    for(key in _this.pcoadata) {
                        plotpcoak[klabels[key]].push(_this.pcoadata[key]);
                        series.push({label: "Cluster "+(parseInt(key)+1)});
                    }
                    _this.pcoaplot.destroy();
                    _this.pcoaoptions["series"] = series;
                    _this.pcoaoptions.legend = {
                        show: true,
                        renderer: $.jqplot.EnhancedLegendRenderer,
                        rendererOptions: {
                            numberRows: 20
                        },
                        placement : "outside"
                    };
                    _this.pcoaplot = $.jqplot('pcoaplot', getPCs_col(plotpcoak, _this.current_pcs[0], _this.current_pcs[1]), _this.pcoaoptions);
                },
                dataType: "json"
            });
        });


        //save pcoa
        $("#savepcoa").click(function() {
            $('#pcoaplot').jqplotSaveImage();
        });


        //toggle pcoa cluster coloring based on k-means data
        $("#pcoacol").change(function() {
            if($(this).is(":checked")) {
                //change coloring select entry
                $(".pcoacol option[value='kmeans']").prop('selected', 'selected');
                var plotpcoak = [];
                series = []
                for(key in kmeans) {
                    var kseries = [];
                    //TODO: ugly and slow code (better way?)
                    for( i in kmeans[key]) {
                        for( j in _this.pcoadata) {
                            if(kmeans[key][i] == _this.pcoadata[j][3]) {
                                kseries.push(_this.pcoadata[j]);
                                continue;
                            }
                        }
                    }
                    plotpcoak.push(kseries);
                    series.push({label: "Cluster "+(parseInt(key)+1)});
                }
                _this.pcoaplot.destroy();
                _this.pcoaoptions["series"] = series;
                _this.pcoaoptions.legend = {
                    show: true,
                    renderer: $.jqplot.EnhancedLegendRenderer,
                    rendererOptions: {
                        numberRows: 20
                    },
                    placement : "outside"
                };
                _this.pcoaplot = $.jqplot('pcoaplot', getPCs_col(plotpcoak, _this.current_pcs[0], _this.current_pcs[1]), _this.pcoaoptions);
            }
            else {
                //change coloring select entry
                $(".pcoacol option[value='nocol']").prop('selected', 'selected');
                _this.pcoaplot.destroy();
                _this.pcoaoptions.legend = { show: false };
                _this.pcoaplot = $.jqplot('pcoaplot', [getPCs(_this.pcoadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcoaoptions);
            }
        });


        //change color in pcoa by dropdown
        $(".pcoacol").change(function() {
            if($(".pcoacol").select2("val") == "nocol") {
                //reset coloring
                _this.pcoaplot.destroy();
                _this.pcoaoptions.legend = { show: false };
                _this.pcoaplot = $.jqplot('pcoaplot', [getPCs(_this.pcoadata,_this.current_pcs[0],_this.current_pcs[1])], _this.pcoaoptions);
            }
            else {
                $.ajax({
                    type: "POST",
                    data: {
                        feature: $(".pcoacol").select2("val")
                    },
                    success: function(data) {
                        var features = data;
                        //change col
                        var plotpcoak = [];
                        series = [];
                        foundFeatures = [];
                        for(key in features) {
                            var fseries = [];
                            //TODO: ugly and slow code (better way?)
                            for( i in features[key]) {
                                for( j in _this.pcoadata) {
                                    if(features[key][i] == _this.pcoadata[j][3]) {
                                        foundFeatures.push(_this.pcoadata[j][3]);
                                        fseries.push(_this.pcoadata[j]);
                                        continue;
                                    }
                                }
                            }
                            if(typeof fseries != "undefined" && fseries != null && fseries.length > 0) {
                                plotpcoak.push(fseries);
                                if(!isNaN(key)) {
                                    if(parseInt(key) == 0) {
                                        key = "0";
                                    }
                                    else {
                                        key = Math.round(key*Math.pow(10,3))/Math.pow(10,3);
                                    }
                                }
                                series.push({label: key});
                            }
                        }
                        //add all points not in features
                        var fseries = [];
                        for(item in _this.pcoadata) {
                            if(foundFeatures.indexOf(_this.pcoadata[item][3]) < 0) {
                                fseries.push(_this.pcoadata[item]);
                            }
                        }
                        plotpcoak.push(fseries);
                        series.push({label: "No metadata"});
                        _this.pcoaplot.destroy();
                        _this.pcoaoptions["series"] = series;
                        _this.pcoaoptions.legend = {
                            show: true,
                            renderer: $.jqplot.EnhancedLegendRenderer,
                            rendererOptions: {
                                numberRows: 20
                            },
                            placement : "outside"
                        };
                        _this.pcoaplot = $.jqplot('pcoaplot', getPCs_col(plotpcoak, _this.current_pcs[0], _this.current_pcs[1]), _this.pcoaoptions);
                    },
                    dataType: "json"
                });
            }
        });
    });

}