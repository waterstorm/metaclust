# MetaClust #
MetaClust is a web-application for quick and easy clustering of even large metagenomics datasets.

### Installation ###
A manual how to install and use this application can also be found in this git [repository](https://bitbucket.org/waterstorm/metaclust/src/master/metacluster/static/help.pdf?at=master).

### Contact ###
This Web-application was developed by Jens Rauch at the University of Tuebingen as a Master Thesis. Feel free to report bugs using the issue tracker in this GIT repository.

### Terms of use ###
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.	
You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses.